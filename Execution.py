import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from celluloid import Camera
from Seguimiento1 import Particulas
from IPython.display import HTML

if __name__ == '__main__':
    np.random.seed(0)

    m = 1.  #Masa
    B = np.array([-5,2,1])  #Campo magnético
    q1 = -1.; q2 = 1.  #Cargas
    r01 = np.array([1,0,0]); r02 = np.array([0,1,0])  #Posiciones iniciales
    v01 = np.random.random()*(r02-r01)/np.linalg.norm((r02-r01))
    v02 = -np.random.random()*(r02-r01)/np.linalg.norm((r02-r01))  #Velocidades iniciales
    a01 = np.array([0,0,0]); a02 = np.array([0,0,0])  #Aceleraciones iniciales

    tf = 10
    dt = 0.01
    t = np.arange(0,tf,dt)   #Arreglo de tiempos

    #Aceleraciones, velocidades y posiciones de las dos partículas:

    n = len(t)
    a1 = np.zeros((n,3)); v1 = np.zeros((n,3)); r1 = np.zeros((n,3))
    a2 = np.zeros((n,3)); v2 = np.zeros((n,3)); r2 = np.zeros((n,3))

    a1[0] = a01; a2[0] = a02; v1[0] = v01; v2[0] = v02; r1[0] = r01; r2[0] = r02

    fig_anim = plt.figure()
    ax_anim = fig_anim.add_subplot(projection='3d')
    cam = Camera(fig_anim)

    for i in range(0,n-1):

        Part1 = Particulas(m,q1,r1[i],v1[i],a1[i])
        Part2 = Particulas(m,q2,r2[i],v2[i],a2[i])
        a1[i+1], v1[i+1], r1[i+1] = Part1.movimiento_particula(0.01,B,Part2)
        a2[i+1], v2[i+1], r2[i+1] = Part2.movimiento_particula(0.01,B,Part1)
        ax_anim.plot(r1[i,0],r1[i,1],r1[i,2],marker='.',color='r')
        ax_anim.plot(r2[i,0],r2[i,1],r2[i,2],marker='.',color='b')

        cam.snap()
        plt.close()

    fig_pos, ax_pos = plt.subplots(4,1,figsize=(10,22))
    ax_pos[0].plot(t,r1[:,0],color='r',label=r'$q_1$')
    ax_pos[1].plot(t,r1[:,1],color='r',label=r'$q_1$')
    ax_pos[2].plot(t,r1[:,2],color='r',label=r'$q_1$')
    ax_pos[0].plot(t,r2[:,0],color='b',label=r'$q_2$')
    ax_pos[1].plot(t,r2[:,1],color='b',label=r'$q_2$')
    ax_pos[2].plot(t,r2[:,2],color='b',label=r'$q_2$')
    ax_pos[3].plot(t,np.linalg.norm(r1-r2,axis=1),color='purple')
    ax_pos[0].set(title=r'$x(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$x(t)$ [cm]')
    ax_pos[1].set(title=r'$y(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$y(t)$ [cm]')
    ax_pos[2].set(title=r'$z(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$z(t)$ [cm]')
    ax_pos[3].set(title=r'$r_{12}(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$r_{12}(t)$ [cm]')
    ax_pos[0].grid()
    ax_pos[1].grid()
    ax_pos[2].grid()
    ax_pos[3].grid()
    ax_pos[0].legend()
    ax_pos[1].legend()
    ax_pos[2].legend()
    plt.subplots_adjust(wspace=0.2,
                    hspace=0.3)
    fig_pos.savefig('posiciones.png')  # Guarda la gráfica
# Guarda la gráfica
    plt.show()

    # Gráfica de las componentes de la velocidad
    fig_vel, ax_vel = plt.subplots(3,1,figsize=(10,16))
    ax_vel[0].plot(t,v1[:,0],color='r',label=r'$q_1$')
    ax_vel[1].plot(t,v1[:,1],color='r',label=r'$q_1$')
    ax_vel[2].plot(t,v1[:,2],color='r',label=r'$q_1$')
    ax_vel[0].plot(t,v2[:,0],color='b',label=r'$q_2$')
    ax_vel[1].plot(t,v2[:,1],color='b',label=r'$q_2$')
    ax_vel[2].plot(t,v2[:,2],color='b',label=r'$q_2$')
    ax_vel[0].set(title=r'$v_x(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$v_x(t)$ [cm]')
    ax_vel[1].set(title=r'$v_y(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$v_y(t)$ [cm]')
    ax_vel[2].set(title=r'$v_z(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$v_z(t)$ [cm]')
    ax_vel[0].grid()
    ax_vel[1].grid()
    ax_vel[2].grid()
    ax_vel[0].legend()
    ax_vel[1].legend()
    ax_vel[2].legend()
    plt.subplots_adjust(wspace=0.2,
                    hspace=0.3)
    fig_vel.savefig('velocidades.png')  # Guarda la gráfica
    plt.show()


    # Gráfica de las componentes de la aceleración
    fig_acc, ax_acc = plt.subplots(3,1,figsize=(10,16))
    ax_acc[0].plot(t,a1[:,0],color='r',label=r'$q_1$')
    ax_acc[1].plot(t,a1[:,1],color='r',label=r'$q_1$')
    ax_acc[2].plot(t,a1[:,2],color='r',label=r'$q_1$')
    ax_acc[0].plot(t,a2[:,0],color='b',label=r'$q_2$')
    ax_acc[1].plot(t,a2[:,1],color='b',label=r'$q_2$')
    ax_acc[2].plot(t,a2[:,2],color='b',label=r'$q_2$')
    ax_acc[0].set(title=r'$a_x(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$a_x(t)$ [cm]')
    ax_acc[1].set(title=r'$a_y(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$a_y(t)$ [cm]')
    ax_acc[2].set(title=r'$a_z(t)$ vs $t$',xlabel=r'$t$ [s]',ylabel=r'$a_z(t)$ [cm]')
    ax_acc[0].grid()
    ax_acc[1].grid()
    ax_acc[2].grid()
    ax_acc[0].legend()
    ax_acc[1].legend()
    ax_acc[2].legend()
    plt.subplots_adjust(wspace=0.2,
                    hspace=0.3)
    fig_acc.savefig('aceleraciones.png')  # Guarda la gráfica
    plt.show()

    anim = cam.animate(interval = 15)
    anim.save('anim.gif')  # Guarda la animación
    HTML(anim.to_html5_video())
    #Gráfica con las trayectorias:
  
    fig = plt.figure (figsize = (8.5, 8.5))
    axis = fig.add_subplot (projection = '3d')
    axis.plot (r1[:,0], r1[:,1], r1[:,2], linewidth = 0.5, color = 'red')
    axis.plot (r2[:,0], r2[:,1], r2[:,2], linewidth = 0.5, color = 'blue')
    axis.set_xlabel('<--- x(t) --->',)
    axis.set_ylabel('<--- y(t) --->',)
    axis.set_zlabel('<--- z(t) --->',)
    axis.set_title('Trayectoria de las partículas cargadas',)
    fig.savefig('trayectoria.png') 
    plt.show()
